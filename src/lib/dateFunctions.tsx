import dayjs from 'dayjs'
import localeData from 'dayjs/plugin/localeData'
import 'dayjs/locale/ru'
import weekdays from 'dayjs/plugin/weekday';
import isToday from 'dayjs/plugin/isToday';
import isBetween from 'dayjs/plugin/isBetween';

dayjs.extend(isBetween);
dayjs.extend(localeData);
dayjs.extend(isToday);
dayjs.locale('ru');

export const convertDate = (milliseconds: number): DateConverted => {

    const date = dayjs(milliseconds);

    return {
        weekDay: dayjs.localeData().weekdays()[date.day()],
        dayAndMonth: date.format('DD MMMM')
    }
};

export const checkIsToday = (milliseconds: number): boolean => {
    return dayjs(milliseconds).isToday();
};

export const checkDayBelongsToCurrentWeek = (milliseconds: number): boolean => {
    return dayjs(milliseconds).isBetween(dayjs().subtract(1, 'd'), dayjs().add(6, 'd'));
};

interface DateConverted {
    weekDay: string;
    dayAndMonth: string;
}