/* Core */
import { createContext, useState, SetStateAction } from 'react';

import {WeatherModel} from "../mock-data/Forecast.class";

export const WeatherContext = createContext<WeatherProviderShape>([
    {
        id: '',
        type: '',
        temperature: 0,
        rain_probability: 0,
        day: 0,
        humidity: 0
    },
    () => null,
]);

export const WeatherProvider: React.FC = props => {
    const state = useState<WeatherModel>({
        id: '',
        type: '',
        temperature: 0,
        rain_probability: 0,
        day: 0,
        humidity: 0
    });

    return <WeatherContext.Provider value={state}>{props.children}</WeatherContext.Provider>;
};

/* Types */
type WeatherProviderShape = [WeatherModel, React.Dispatch<SetStateAction<WeatherModel>>];
