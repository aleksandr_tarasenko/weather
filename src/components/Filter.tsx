/* Core */
import { useFormik } from 'formik';
import cx from 'classnames';
import {WeatherResponse} from "../api/api";
import {useState} from "react";
import {FilterDays} from "../storages/ForecastStore";
import {store} from "../storages";

/* Components */

export const Filter: React.FC<Props> = ({forecastData}) => {
    let [filtered, setFiltered] = useState(false);
    const {forecast, weather} = store;

    const formik = useFormik({
        initialValues: {
            cloudy: false,
            sunny: false,
            minTemperature: 0,
            maxTemperature: 0,
        },
        onSubmit: (values) => {
            const filterData: FilterDays = {
                type: values.cloudy && formik.getFieldMeta('cloudy').touched ? 'cloudy'
                    : values.sunny && formik.getFieldMeta('sunny').touched ? 'sunny' : undefined,
                minTemperature:  formik.getFieldMeta('minTemperature').touched ? values.minTemperature : undefined,
                maxTemperature: formik.getFieldMeta('maxTemperature').touched ? values.maxTemperature : undefined
            };

            const days = forecast.filterDays(forecastData?.data, filterData);
            days.length && weather.setToday(days);

            setFiltered(true);
        },
    });

    const switchCheckboxesState = (state: boolean) => {
        formik.setFieldValue('cloudy', state);
        formik.setFieldValue('sunny', !state);
    };

    const isAnyFieldTouched = formik.getFieldMeta('cloudy').touched
        || formik.getFieldMeta('sunny').touched
        || formik.getFieldMeta('minTemperature').touched
        || formik.getFieldMeta('maxTemperature').touched;

    const checkboxJSX = (fieldName: string, rusFieldName: string, setValue: boolean) => {
        return <span className={cx('checkbox',
            {
                selected: formik.getFieldProps(fieldName).value,
                blocked: filtered
            })}
                 onClick={() => {
                     filtered && switchCheckboxesState(setValue) && formik.setFieldTouched(fieldName, true);
                 }}
        >
            {rusFieldName}
            </span>
    };

    const numberFieldJSX = (id: string, fieldName: string, rusFieldName: string) => {
        return <p className="custom-input">
            <label htmlFor={id}>{rusFieldName}</label>
            <input id={id}
                   type="number"
                   onChange={(min) => {
                       formik.setFieldValue(fieldName, min.target.value);
                       formik.setFieldTouched(fieldName, true)
                   }}
                   disabled={filtered}
            />
        </p>;
    };

    const onReset = () => {
        forecastData && forecast.setSevenDays(forecastData.data);
        forecastData && weather.setToday(forecastData.data);
        formik.resetForm();
        setFiltered(false);
    };

    return (
        <form className='filter' onSubmit={formik.handleSubmit}>
            {checkboxJSX('cloudy', 'Облачно', true)}
            {checkboxJSX('sunny', 'Солнечно', false)}
            {numberFieldJSX('min-temperature', 'minTemperature', 'Минимальная температура')}
            {numberFieldJSX('max-temperature', 'maxTemperature', 'Максимальная температура')}
            {!filtered ? <button disabled={!isAnyFieldTouched} type='submit'>Отфильтровать</button> :
                <button onClick={onReset} type="reset">Сбросить</button>}
        </form>
    );
};

interface Props {
    forecastData?: WeatherResponse;
}