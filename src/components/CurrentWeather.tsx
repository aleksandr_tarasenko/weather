/* Core */

/* Components */

import {store} from "../storages";
import {observer} from "mobx-react-lite";

export const CurrentWeather: React.FC = observer(() => {
    const {forecast} = store;
    const weather = store.weather.getWeather;

    return forecast.Days.length ? (
        <div className='current-weather'>
            <p className='temperature'>
                {weather.temperature}
            </p>
            <p className='meta'>
                <span className='rainy'>
                    % {weather.rain_probability}
                </span>
                <span className='humidity'>
                    % {weather.humidity}
                </span>
            </p>
        </div>
    ) : null;
});
