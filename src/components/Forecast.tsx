/* Components */
import {Day} from "./Day";
import {WeatherResponse} from "../api/api";
import {observer} from "mobx-react-lite";
import {useEffect} from "react";
import {store} from "../storages";

export const Forecast: React.FC<Props> = observer(({forecast}) => {
    const {forecast: forecastStore, weather} = store;
    const data: any = forecast?.data;

    useEffect(() => {
        forecastStore.setSevenDays(data);
        weather.setToday(data);
    }, []);

    const forecastJSX = forecastStore.Days.length ? forecastStore.Days.map((current, index) => {
            return <Day key={index} data={current}/>
        }) : <p className="message">По заданным критериям нет доступных дней!</p>;

    return (
        <div className='forecast'>
            {forecastJSX}
        </div>
    );
});

type Props = {
    forecast?: WeatherResponse;
}