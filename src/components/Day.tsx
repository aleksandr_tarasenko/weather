/* Components */
import {WeatherModel} from "../mock-data/Forecast.class";
import {convertDate} from "../lib/dateFunctions";
import {store} from "../storages";
import {observer} from "mobx-react-lite";

export const Day: React.FC<Props> = observer(({data}) => {
    const weather = store.weather;
    const weekDay = convertDate(data.day).weekDay;

    return (
        <div className={`day ${data.type} ${weather.getWeather.id === data.id ? 'selected' : ''}`}
             onClick={() => weather.setWeather(data)}
        >
            <p>{weekDay}</p>
            <span>
                {data.temperature}
            </span>
        </div>
    );
});

type Props = {
    data: WeatherModel;
}