/* Core */

/* Components */

import {convertDate} from "../lib/dateFunctions";
import {store} from "../storages";
import {observer} from "mobx-react-lite";

export const Head: React.FC = observer(() => {
    const {forecast} = store;
    const weather = store.weather.getWeather;
    const date = convertDate(weather.day);

    return forecast.Days.length ? (
        <div className='head'>
            <div className={`icon ${weather.type}`}/>
            <div className='current-date'>
                <p>{date.weekDay}</p>
                <span>{date.dayAndMonth}</span>
            </div>
        </div>
    ) : null;
});
