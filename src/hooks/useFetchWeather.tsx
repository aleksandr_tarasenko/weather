/* Core */
import { useQuery } from 'react-query';

/* Instruments */
import { api } from '../api';

export const useFetchWeather = () => {
    return useQuery('weather', api.getWeather);
};
