/* Core */
import axios from "axios";
import {WeatherModel} from "../mock-data/Forecast.class";

const WEATHER_API_URL = process.env.REACT_APP_WEATHER_API_URL;

export type WeatherResponse = {
    data: Required<WeatherModel[]>;
}

export const api = {
    async getWeather(): Promise<Required<WeatherResponse>> {
        const response = await axios.get(`${WEATHER_API_URL}`);

        return response.data;
    },
};
