/* Core */

/* Components */
import { Head, CurrentWeather, Forecast, Filter } from './components';
import * as hooks from './hooks';

export const App: React.FC = () => {
    const fetchWeatherQ = hooks.useFetchWeather();

    return (
        <main>
            {fetchWeatherQ.isLoading ? ' загрузка данных...' : <>
                <Head />
                <CurrentWeather/>
                <Forecast forecast={fetchWeatherQ.data}/>
                <Filter forecastData={fetchWeatherQ.data}/>
                </>}
        </main>
    );
};
