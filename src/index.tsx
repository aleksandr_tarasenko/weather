/* Core */
import { render } from 'react-dom';

/* Components */
import { App } from './App';

/* Instruments */
import './theme/index.scss';
import {WeatherProvider} from "./lib/weatherContext";
import {QueryClientProvider} from "react-query";
import { queryClient } from './hooks';

render(
    <QueryClientProvider client={queryClient}>
            <App />
    </QueryClientProvider>,
    document.getElementById('root'));
