import {ForecastStore, forecastStore} from './ForecastStore';
import {WeatherStore, weatherStore} from './WeatherStore';
import {types} from "mobx-state-tree";

const RootStore = types.model('RootStore', {
    forecast: ForecastStore,
    weather: WeatherStore
});

export const store = RootStore.create({
    weather: weatherStore,
    forecast: forecastStore,
});