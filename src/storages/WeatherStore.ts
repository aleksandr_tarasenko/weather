/* Core */
import {WeatherModel} from "../mock-data/Forecast.class";
import {checkIsToday} from "../lib/dateFunctions";
import {types} from "mobx-state-tree";

export const Weather = types
    .model('Weather', {
        id: types.identifier,
        rain_probability: types.number,
        humidity: types.number,
        day: types.number,
        temperature: types.number,
        type: types.string,
    });

export const WeatherStore = types
    .model('WeatherStore', {
        weather: Weather,
    })
    .actions((self) => {
        const setWeather = (value: WeatherModel) => {
            self.weather = {...value};
        };

        const setToday = (value: WeatherModel[]) => {
            const newWeather: WeatherModel | undefined = value.find(date => checkIsToday(date.day));

            setWeather(newWeather || value[0]);
        };

        return {setWeather, setToday}
    })
    .views((self) => ({
        get getWeather() {
            return self.weather;
        }
    }));

export const weatherStore = WeatherStore.create({
    weather: {
        id: '',
        type: '',
        temperature: 0,
        rain_probability: 0,
        day: 0,
        humidity: 0
    }
});