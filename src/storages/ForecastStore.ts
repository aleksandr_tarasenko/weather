/* Core */
import {WeatherModel} from "../mock-data/Forecast.class";
import {checkDayBelongsToCurrentWeek} from "../lib/dateFunctions";
import {types} from 'mobx-state-tree';
import {Weather} from "./WeatherStore";

export const ForecastStore = types
    .model('ForecastStore', {
        days: types.array(Weather),
    })
    .actions((self) => ({
        setSevenDays (value: any) {
            self.days = value ? value.filter((element: WeatherModel) => checkDayBelongsToCurrentWeek(element.day)) : [];
        },
        filterDays(data: any, filters: FilterDays ) {
            if(filters.type) data = data.filter((value: WeatherModel) => {
                return value.type === filters.type;
            });
            if(filters.minTemperature) data = data.filter((value: WeatherModel) => {
                return  filters.minTemperature ? value.temperature > filters.minTemperature : value;
            });
            if(filters.maxTemperature) data = data.filter((value: WeatherModel) => {
                return filters.maxTemperature ? value.temperature < filters.maxTemperature : value;
            });

            self.days = data ? data.filter((element: WeatherModel) => checkDayBelongsToCurrentWeek(element.day)) : [];
            return self.days;
        }
    }))
    .views((self) => ({
        get Days() {
            return self.days;
        }
    }));

export const forecastStore = ForecastStore.create({
    days: []
});

export type FilterDays = {
    type?: string;
    minTemperature?: number;
    maxTemperature?: number;
}